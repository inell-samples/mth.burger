import style from "./Container.module.css";
import classNames from "classnames"; 

export const Container = ({children, className}) => (
    // Обращение к калссу испортированного объекта стилей (создает уникальное имя класса для данного объекта)
    <div className={classNames(style.container, className)}>
        {children}
    </div>
);