import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { API_URI, POSTFIX } from "../../const";

const initialState = {
    category: [],
    error: '',
    activeCategory: 0,
};

export const categoryRequestAsync = createAsyncThunk('category/fetch', (data, obj) => {
    return fetch(`${API_URI}${POSTFIX}/category`)
    .then(req => req.json())
    // Возвращаем объект ошибки
    .catch(err => ({err}));
})

const categorySlice = createSlice({
    name: "category",
    initialState: initialState,
    // Это actrions
    reducers: {
        changeCategory(state, action) {
            state.activeCategory = action.payload.indexCategory;
        }
    },
    extraReducers: builder => {
        builder
            .addCase(categoryRequestAsync.pending, (state) => {
                state.error = '';
            })
            .addCase(categoryRequestAsync.fulfilled, (state, action) => {
                state.error = '';
                state.category = action.payload;
            })
            .addCase(categoryRequestAsync.rejected, (state, action) => {
                state.error = action.payload;
            })
    }
    // Old notation
    /*extraReducers: {
        [categoryRequestAsync.pending.type]: (state) => {
            state.error = '';
        },
        [categoryRequestAsync.fulfilled.type]: (state, action) => {
            state.error = '';
            state.category = action.payload;
        },
        [categoryRequestAsync.rejected.type]: (state, action) => {
            state.error = action.payload;
        }
    }*/
});

export const { changeCategory } = categorySlice.actions

export default categorySlice.reducer;