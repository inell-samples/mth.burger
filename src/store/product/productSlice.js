import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { API_URI, POSTFIX } from "../../const";

const initialState = {
    products: [],
    error: '',
    flag: false
};

export const productRequestAsync = createAsyncThunk(
    "product/fetch", (category) => 
        fetch(`${API_URI}${POSTFIX}?category=${category}`)
            .then(req => req.json())
            // Возвращаем объект ошибки
            .catch(err => ({err}))
)

const productSlide = createSlice({
    name: "product",
    initialState,
    extraReducers: builder => {
        builder
            // Перечисляются состояния промиса
            .addCase(productRequestAsync.pending, state => {
                state.error = '';
                state.flag = false;
            })
            .addCase(productRequestAsync.fulfilled, (state, action) => {
                state.error = '';
                state.products = action.payload;
                state.flag = true;
            })
            .addCase(productRequestAsync.rejected, (state, action) => {
                state.error = action.payload.err;
            })
    }
});

export default productSlide.reducer;